package com.example.pafi.weatherapp.RestAPI;

import com.example.pafi.weatherapp.RestAPI.Result;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;


public interface ResultAPI {
    @GET("2.5/forecast/city?APPID=db68600e3e4f3da625ec70b4364eb014")
    Call<Result> getByLocation(@Query("lat") double lat, @Query("lon") double lon);

    @GET("2.5/forecast/city?APPID=db68600e3e4f3da625ec70b4364eb014")
    Call<Result> getByCity(@Query("q") String name);
}
